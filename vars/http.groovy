//import groovy.json.JsonOutput;
//node ("master") {
//@Library('http') _
//http(['url' : "https://jira.com/rest/api/2/search",
//		'http_method' : "POST",
//		'credential_id' : "domain_",
//		'content_type' : "application/json",
//		'parameters' : JsonOutput.toJson(['jql':"project = XYZ"]),
//		'acceptable_response_codes':[200,201]])
//}

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.UUID;
import groovy.json.JsonSlurperClassic;

def call(config) {

String encoding = ""

withCredentials([usernamePassword(credentialsId: config.credential_id, passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
encoding = Base64.getEncoder().encodeToString((USERNAME+":"+PASSWORD).getBytes("utf-8"));
}

URL url = new URL(config.url);
HttpURLConnection connection = (HttpURLConnection) url.openConnection();
connection.setRequestMethod(config.http_method);
connection.setRequestProperty("Content-Type", config.content_type);
connection.setRequestProperty("Authorization", "Basic " + encoding);
connection.setDoOutput(true);
switch (config.http_method) {
case "POST":
	OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
	osw.write(config.parameters);
	osw.flush();
	osw.close();
	break;
case "GET":
	break;
case "PUT":
	OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
	osw.write(config.parameters);
	osw.flush();
	osw.close();
	break;
}

int responseCode = connection.getResponseCode();
println("Response Code : " + responseCode);

InputStream stream = connection.getErrorStream();
if (stream == null) {
stream = connection.getInputStream();
}
BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
String result = reader.readLine();
reader.close();

connection.disconnect();

if (!config.acceptable_response_codes.contains(responseCode)) {
println("Response String : " + result)
throw new Exception("Unacceptable response code")
}

jsonObj = null
if (result) {
	jsonObj = new JsonSlurperClassic().parseText(result);
}
return jsonObj;
}

